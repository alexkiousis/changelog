from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone

from .forms import PostForm
from .models import Post


def index(request):
    post_list = Post.objects.filter(published_date__isnull=False).order_by(
        "-published_date"
    )
    context = {"post_list": post_list}
    return render(request, "posts/index.html", context)


def detail(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    return render(request, "posts/detail.html", {"post": post})


def submit(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.created_date = timezone.now()
            post.save()
            return redirect("index")
    else:
        form = PostForm()
    return render(request, "posts/submit.html", {"form": form})


def unpublished(request):
    unpublished_post_list = Post.objects.filter(published_date__isnull=True)
    context = {"unpublished_post_list": unpublished_post_list}
    return render(request, "posts/unpublished.html", context)
