from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("submit/", views.submit, name="submit"),
    path("unpublished/", views.unpublished, name="unpublished"),
    path("links/<post_id>/", views.detail, name="detail"),
]
