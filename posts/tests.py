from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from .models import Post

import uuid


class PostModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username="admin", password="pass@123", email="admin@example.com"
        )
        create_post = Post.objects.create(
            id=uuid.uuid4(),
            author=create_user,
            url="http://example.com",
            title="Example",
        )

    def test_model_post_str(self):
        post = Post.objects.filter(title="Example").first()
        self.assertEqual(post.__str__(), post.title)


class PostViewTest(TestCase):
    def test_posts_index_view(self):
        response = self.client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)

    def test_post_detail_view(self):
        create_user = User.objects.create(username="testxxx", email="test@example.com")
        create_post = Post.objects.create(
            id=uuid.uuid4(),
            author=create_user,
            url="http://example.com",
            title="Example",
            text="Test",
        )
        response = self.client.get(
            reverse("detail", kwargs={"post_id": create_post.id})
        )
        self.assertEqual(response.status_code, 200)

    def test_post_create_get_view(self):
        response = self.client.get(reverse("submit"))
        self.assertEqual(response.status_code, 200)

    def test_post_create_POST_view(self):
        login = self.client.login(username="testxxx", password="test")
        response = self.client.post(
            reverse("submit"),
            {"url": "http://example.com", "title": "Example", "text": "test"},
        )
        self.assertEqual(response.status_code, 200)

    def test_posts_unpublished_view(self):
        response = self.client.get(reverse("unpublished"))
        self.assertEqual(response.status_code, 200)
