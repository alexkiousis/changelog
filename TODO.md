# Project roadmap

- Create `publishing` functionality (admin approves submitions)
- Implement Markdown support for Post notes
- Make index list layout more spacious
- Create full profile pages (change password, reset_password, 2fa)
- Create link parser endpoint and functionality in submit form
- Search
- Social auth login
- Tagging
- Create RSS feed for main page
- Create automated newsletters (daily, weekly) based on submissions
