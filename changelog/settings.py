"""
Django settings for changelog project.
"""
import os
import dj_database_url

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEBUG = os.getenv("CHANGELOG_DEBUG", "False")
SECRET_KEY = os.environ.get("CHANGELOG_SECRET_KEY", None)
HOSTS_CONF = os.environ.get("CHANGELOG_HOSTS", "").split(",")
ALLOWED_HOSTS = list(filter(lambda s: len(s) > 0, HOSTS_CONF))

ROOT_URLCONF = "changelog.urls"
WSGI_APPLICATION = "changelog.wsgi.application"
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")
LOGIN_REDIRECT_URL = "index"
LOGOUT_REDIRECT_URL = "index"

# Parse SQL database URL from environment
# See https://github.com/jacobian/dj-database-url#usage for valid URLs
CHANGELOG_DATABASE_DEFAULT = "sqlite:///{}".format(os.path.join(BASE_DIR, "db.sqlite3"))
CHANGELOG_DATABASE_URL = os.environ.get(
    "CHANGELOG_DATABASE_URL", CHANGELOG_DATABASE_DEFAULT
)
DATABASES = {"default": dj_database_url.parse(CHANGELOG_DATABASE_URL)}

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "widget_tweaks",
    "accounts",
    "posts",
]
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    'whitenoise.middleware.WhiteNoiseMiddleware',
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]
