#!/bin/sh

. ${OPTION_VENVDIR}/bin/activate

./manage.py collectstatic --noinput

gunicorn changelog.wsgi \
    --bind=[::]:${OPTION_GUNICORN_PORT} \
    --workers=${OPTION_GUNICORN_WORKERS} \
    --worker-tmp-dir=/dev/shm \
    --log-file=- \
    --access-logfile=-
